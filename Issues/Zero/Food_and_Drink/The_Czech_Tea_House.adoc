The Czech Tea House
Christopher Rozruch <krozruch@pocketapocketa.cz>

On the train back from Usti nad Labem I read the New Yorker. A group next to us sat with their legs stretched out. The first was wearing a Slavie top. Not a good sign, I thought, and questioned Woodstock's wisdom sitting down so close to them, though there was little enough choice. One talked about how drunk he was. He would not come around even by the next day, Monday, he thought.

We had spent the day touring around the hills surrounding the industrial town, talking about how it is to come from a town that smells so bad - this is a stink, nothing less. I compared it to the Black Country. It was a bright autumn day - one of the first sunny days Woodstock had known in the town - and the hills and mixed forests up above the morning inversion were suspiciously short of life looking back now, but beautiful nonetheless. We talked about mixed forests, looking over all of their subdued reds and oranges and yellows now, and how the monoculture woodlands of Šumava in the East are depressing, and perhaps would be even if they were not ravaged by the barrk beetle.

We had come down to into the town prematurely after having a packed lunch up in a wooden hide with broken windows and finding ourselves doubling back on ourselves. We walked around a little, listened to a romany guy swearing about bus drivers, came down to the stink of the "bone flour" they were boiling up, to a train coming out of a factory onto the street we were walking on, and reversing again. I was glad we had come and would have spent more time talking around but I was out of money entirely, I was shooting film, we were carrying our heavy bags, and we decided to take a late train and sit in a tea house for a while.

I had been in a tea house the day before, drinking green tea, smoking a water pipe, looking over a repo for a web journal I was planning to release. Woodstock had then joined me, we had sat for another hour or two before grabbing dinner and going once again to a different, more punk tea house for a reggae night in the evening.

We had last been in a tea house in Uherske hradiste. Embarrassed by a friend who works for a huge Swedish firm. "Vařím čaj". Travelling into the Usti where Woodstock dropped me off to get off to her fashion show, I first saw a Dobrá čajovna upon entering the town. It brought me back.

I can't remember the first time I went to Dobrá čajovna on Wenceslas Square or who it was with.

Things have changed since 2013. Back then there were a lot of dreďáci, alternative types who could work there. The lads in Dobrá čajovna on Wenceslas Square talked about mountain bikes (not yet a big deal here), looked like they had a contact or two for marijuana, and played up the gong at the end of the day where they spoke in an incomprehensible monotone in Czech before switching to English.

I had struggled with the alcoholic culture of Britain for many years yet, and would struggle with the alcoholic culture of the Czech Republic for some time yet.

It is not enough to choose not to drink. It does so much to be surrounded by people who are making the same choice. In the second punk čajovna, the atmosphere was undoubtedly better than in a pub but not yet so relaxed as in the first. Still, we had a back room and had a better time than I expected given as I am not used to sitting around and conversing. It's not something I am good at. 

We were returning on the day of the Austrian elections. I had read a little about it that morning drinking some of the that horrible tea bag tea we are used to in Britain. The Czech versions are worse. Now the drunken guy was checking his phone and welcoming the result. The [] have got 27%, he said. To je dobry. They are fascists, he said. A little order.

Nejlepsi politicka strana je pivo knedloveprozeli.

We were coming back, in part, to our two cannabis plants on our windowsill. I write elsewhere about what marijuana means to me. Walking back I thought about Martin Konvička and the drink-loving types surrounding the anti-refugee and neo-fascist movements discussed by Jamie Bartlett in Radicals. I think about Tommie Robinson who throws punches like a man who has thrown a lot of punches in his time. I think about the people at university who used to discuss Britain's drinking culture and how you never find such trouble with people who favour marijuana.

As Woodstock makes an omelette, I read a book on tea that was written by one of the men who set up the Dobrá čajovna chain. His love of tea came about during his time on compulsory military service. He was reacting against the roughness, vulgarity, alcohol, and unfreedom of those days.

I think about tea and tea houses and places that encourage conversation. I talk about the culture of Persia and the countries we have been destroying for so long. I correct myself: they will be male dominated societies, for sure. Still, these people do not drink.

I realise, not for the first time, that our civilisation has a problem with drink. It suppresses thought and conversation both. At least, in the way we know of engaging in it. It can work to some superficial level or in certain restricted contexts, but really, tea, a shisha, perhaps even marijuana biscuits, would be better for that.

I miss it. A sunday, a sabbath, hanging out at a tea house, maybe reading the New Yorker, without a phone, and then going for a film. That could see me right. It is clear to me, the guy who set up this chain has done more for the Czech Republic than anybody else I can think up; the guy who is brewing tea, he is creating a world I would like to live in. For that, I have respect.

CC BY-SA